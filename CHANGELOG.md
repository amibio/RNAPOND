# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## [0.1.1](https://gitlab.inria.fr/amibio/RNAPOND/-/releases/v0.1.1) - 2021-01-11

### Changed
- Adapt Infrared new structure
- Use varnaapi in draw_structure
