 RNAPOND
===

__RNA__ __PO__sitive and __N__egative __D__esign ( __RNAPOND__ ) is a sampling iterative approach which generates design candidates respecting to positive design objectives and negative design principles are captured by introducing disruptive base pairs at each step. The sampling process is built on the design framework [Infrared](https://gitlab.inria.fr/amibio/Infrared).

Installation
---
To use the tool, one could clone the project via `git` or download only the script `RNAPOND.py`.
It requires two libraries to be installed ahead, [RNAlib](https://www.tbi.univie.ac.at/RNA/) from ViennaRNA and [Infrared](https://gitlab.inria.fr/amibio/Infrared).

 
Usage
---
```bash
RNAPOND.py "(((((((....(((...........)))((((((((..(((((((((((((((((((...(((((......))))).)))))).)))))))))))))..))))))))..)))))))" 
```

Further usage information is available via calling `--help``
```bash
❯ RNAPOND.py --help
usage: RNAPOND.py [-h] [-n NSOLUTIONS] [-s NSAMPLES] [-tw TREEWIDTH]
                  [-b NBASEPAIRS] [--seed SEED] [-m {bpp,count}]
                  [--turner1999] [--verbose] [--gcweight GCWEIGHT]
                  [--heavyDinucweight HEAVYDINUCWEIGHT] [--no_initial_dbps]
                  [--dbps DBPS] [-o OUTPUT] [--min_sol MIN_SOL]
                  [--show_progress] [--draw_target] [--varna VARNA]
                  Target

Script to test idea for negative design

positional arguments:
  Target                Target secondary structure to design

optional arguments:
  -h, --help            show this help message and exit
  -n NSOLUTIONS, --nSolutions NSOLUTIONS
                        Numer of design (default: 10)
  -s NSAMPLES, --nSamples NSAMPLES
                        Number of samples at each round (default: 200)
  -tw TREEWIDTH, --treewidth TREEWIDTH
                        Maximum allowed tree width (default: 10)
  -b NBASEPAIRS, --nBasepairs NBASEPAIRS
                        Number of base pairs to forbid at each round (default:
                        3)
  --seed SEED
  -m {bpp,count}, --method {bpp,count}
  --turner1999
  --verbose
  --gcweight GCWEIGHT   GC control for unpaired bases. Set 1 to disable
                        (default: 0.2)
  --heavyDinucweight HEAVYDINUCWEIGHT
                        GC dinucleotide control (default: None)
  --no_initial_dbps     Disable initial disruptive base pairs generation
                        (default: False)
  --dbps DBPS           Initial list of disruptive base pairs. (default: [])
  -o OUTPUT, --output OUTPUT
                        Output folder (default: ./)
  --min_sol MIN_SOL     Minimum number solutions needed in one round to stop
                        updating sampler (default: 1)
  --show_progress       Print progress counter (default: False)
  --draw_target         Draw target with dbps (default: False)
  --varna VARNA         VARNA location (default: VARNAv3-93.jar)
```

