#!/usr/bin/env python3


import sys
import os
from pathlib import Path
import random
import argparse
import json
import logging
from time import time
from varnaapi import VARNA, set_VARNA
# import numpy as np

import RNA

import infrared as ir


START = time()

LOG = logging.getLogger('RNAPOND')
LOG.addHandler(logging.StreamHandler(sys.stdout))


BPS = set([(0, 3), (1, 2), (2, 1), (2, 3), (3, 0), (3, 2)])


## @brief InfraRed Constraint for disruptive base pair
class DisruptiveBp(ir.Constraint):
    def __init__(self, i, j):
        super().__init__([i, j])
        self.i = i
        self.j = j

    def __call__(self, a):
        a = a.values()
        return (a[self.i], a[self.j]) not in BPS

## @brief InfraRed function to control the number of dinucleotides from {G,C}^2
class HeavyDinucleotideControl(ir.Function):
    def __init__(self, i, weight):
        super().__init__([i, i+1])
        self.i = i
        self.weight = weight

    def __call__(self, a):
        a = a.values()

        if (a[self.i], a[self.i+1]) in [ (1,1), (2,2), (1,2), (2,1) ]:
            return self.weight
        else:
            return 1


# Table of special tetra loops with their energy contributions in
# Turner 99 (source:
# https://rna.urmc.rochester.edu/NNDB/turner99/hairpin.html;
# https://rna.urmc.rochester.edu/NNDB/turner99/tloop.txt)
TETRALOOPS_TURNER1999 = [
    ("GGGGAC",  -3),
    ("GGUGAC",  -3),
    ("CGAAAG",  -3),
    ("GGAGAC",  -3),
    ("CGCAAG",  -3),
    ("GGAAAC",  -3),
    ("CGGAAG",  -3),
    ("CUUCGG",  -3),
    ("CGUGAG",  -3),
    ("CGAAGG",  -2.5),
    ("CUACGG",  -2.5),
    ("GGCAAC",  -2.5),
    ("CGCGAG",  -2.5),
    ("UGAGAG",  -2.5),
    ("CGAGAG",  -2),
    ("AGAAAU",  -2),
    ("CGUAAG",  -2),
    ("CUAACG",  -2),
    ("UGAAAG",  -2),
    ("GGAAGC",  -1.5),
    ("GGGAAC",  -1.5),
    ("UGAAAA",  -1.5),
    ("AGCAAU",  -1.5),
    ("AGUAAU",  -1.5),
    ("CGGGAG",  -1.5),
    ("AGUGAU",  -1.5),
    ("GGCGAC",  -1.5),
    ("GGGAGC",  -1.5),
    ("GUGAAC",  -1.5),
    ("UGGAAA",  -1.5)
]

class MotifFunction(ir.Function):
    def __init__(self, i, length, motifs, weight):
        super().__init__(list(range(i,i+length)))
        self.i = i
        self.weight = weight
        self.length = length
        self.motifs = { seq:e for (seq,e) in motifs }
    def __call__(self, a):
        a = a.values()
        motif = ir.rna.values_to_sequence( a[ self.i:self.i + self.length ] )
        try:
            return self.weight**(-self.motifs[motif])
        except:
            return 1

## @brief InfraRed feature to evaluate the sample base pair distance
class BpDistFeature(ir.Feature):
    def __init__(self, structure, weight=5, target=0, tolerance=0):
        super().__init__("Dist", weight, target, tolerance)
        self.targetSS = structure

    def eval(self, sample):
        # mfe = RNA.fold(sample)[0]
        # print(sample)
        # print(self.targetSS)
        return RNA.bp_distance(sample, self.targetSS)

## @brief InfraRed feature to evaluate sample energy to the target structure
class EnergyFeature(ir.Feature):
    def __init__(self, structure, weight=5, target=0, tolerance=0):
        super().__init__("E", weight, target, tolerance)
        self.structure = structure 

    def eval(self, sample):
        return RNA.energy_of_struct(sample, self.structure)

## @brief Function to construct negative design constraint network
def create_negative_cn(target, disruptiveBpList, gcweight, heavyDinucweight):
    seqlen = len(target)
    targetBps = ir.rna.parseRNAStructureBps(target)

    # dependencies = [[i, j] for (i, j) in targetBps] + [[i, j] for (i, j) in disruptiveBpList]
    functions = [ir.rna.BPEnergy(i, j, not(i-1, j+1) in targetBps, 4) for (i,j) in targetBps]

    # add control of GC content for unpaired
    functions.extend([ir.rna.GCControl(i, gcweight) for i,x in enumerate(target) if x=='.'])

    # add control of GC dinucleotides for unpaired (and dangles)
    if heavyDinucweight is not None:
        functions.extend([HeavyDinucleotideControl(i, heavyDinucweight) for i,x in enumerate(target[:-1])
                                if x=='.' or ( x==')' and target[i+1]=='.')])
    tetraLoopWeight = None
    if tetraLoopWeight is not None:
        functions.extend([MotifFunction(i, 6, TETRALOOPS_TURNER1999, tetraLoopWeight) for i,x in enumerate(target[:-5])
                                if target[i:i+6] == "(....)"])

    constraints = [ir.rna.ComplConstraint(i,j) for (i,j) in targetBps] + [DisruptiveBp(i,j) for (i,j) in disruptiveBpList]
    return ir.ConstraintNetwork(varnum=seqlen, domains=4, constraints=constraints, functions=functions)


# @brief InfraRed sampler for negative design
class NegativeSampler(ir.BoltzmannSampler):

    ## @brief Construct with fobidden base pair list
    ## @param target target structure to design
    ## @param disruptiveBpList list of pairs of bases that cannot be paired
    ## @param gcw weight of GC control
    ## @param hvyw weight of heavy gc control
    def __init__(self, target, gcw, disruptiveBpList=[], hvyw=None):
        super().__init__([])

        self.seqlen = len(target)
        self.target = target
        self.disruptiveBpList = disruptiveBpList[:]
        self.gcweight = gcw
        self.heavyDinucweight = hvyw

    ## @return Negative design constraint network
    def gen_constraint_network(self, features):
        return create_negative_cn(self.target, self.disruptiveBpList, self.gcweight, self.heavyDinucweight)

    def gen_tree_decomposition(self, cn):
        return ir.treedecomp.TreeDecompositionFactory().create(cn)

    def sample(self):
        return ir.rna.values_to_sequence(super().sample().values())

## @brief Base pair probability matrix of given sequence
def bpp_from_seq(seq, mfe):
    l = len(seq)
    fc = RNA.fold_compound(seq)
    fc.pf()
    bpp = fc.bpp()
    res = {}
    for i in range(1,l):
        for j in range(i+1,l+1):
            v = bpp[i][j]
            if not v == 0:
                res[(i-1,j-1)] = v
    return res


class BPCount:
    def __init__(self, method="bpp"):
        self.counter = {}
        if method == "bpp":
            self.extract = bpp_from_seq
        elif method == "count":
            self.extract = lambda s, t: {bp: 1 for bp in ir.rna.parseRNAStructureBps(t)}

    def update(self, seq, mfe):
        for bp, v in self.extract(seq, mfe).items():
            if not v == 0:
                self.counter[bp] = self.counter.get(bp, 0) + v

    def get_bp(self):
        lst = sorted(list(self.counter.items()), key=lambda t: t[1], reverse=True)
        return lst


#######################
#                     #
#   Color for dpbs    #
#                     #
#######################

def hex_to_RGB(hex):
    ''' "#FFFFFF" -> [255,255,255] '''
    # Pass 16 to the integer function for change of base
    return [int(hex[i:i+2], 16) for i in range(1,6,2)]


def RGB_to_hex(RGB):
    ''' [255,255,255] -> "#FFFFFF" '''
    # Components need to be integers for hex to make sense
    RGB = [int(x) for x in RGB]
    return "#"+"".join(["0{0:x}".format(v) if v < 16 else
            "{0:x}".format(v) for v in RGB])


def color_dict(gradient):
    ''' Takes in a list of RGB sub-lists and returns dictionary of
      colors in RGB and hex form for use in a graphing function
      defined later on '''
    return {"hex": [RGB_to_hex(RGB) for RGB in gradient],
            "r": [RGB[0] for RGB in gradient],
            "g": [RGB[1] for RGB in gradient],
            "b": [RGB[2] for RGB in gradient]}


def linear_gradient(start_hex, finish_hex="#FFFFFF", n=10):
    ''' returns a gradient list of (n) colors between
      two hex colors. start_hex and finish_hex
      should be the full six-digit color string,
      inlcuding the number sign ("#FFFFFF") '''
    # Starting and ending colors in RGB form
    s = hex_to_RGB(start_hex)
    f = hex_to_RGB(finish_hex)
    # Initilize a list of the output colors with the starting color
    RGB_list = [s]
    # Calcuate a color at each evenly spaced value of t from 1 to n
    for t in range(1, n):
        # Interpolate RGB vector for color at the current value of t
        cv = [int(s[j] + (float(t)/(n-1))*(f[j]-s[j])) for j in range(3)]
        # Add it to our list of output colors
        RGB_list.append(cv)

    return color_dict(RGB_list)

def draw_structure(target, bps, path, varna):
    v = VARNA(structure=target)
    v.set_algorithm('line')
    v.set_bp_style('simple')
    v.set_numeric_params(resolution=4)

    colors = linear_gradient("#ffcccc", "#ff0000", len(bps) + 1)["hex"][1:]
    for bp, c in zip(bps, colors):
        v.add_aux_BP(bp[0], bp[1], color=c)
    v.savefig(path)

def conflict_strings(target,lst):
    cs = list()

    ## test whether bp is in conflict with dot bracket string with brackets []
    def compatible(bp,s):
        if s[bp[0]]!="." or s[bp[1]]!=".":
            return False

        ss = s[bp[0]:bp[1]]
        c=0
        for x in ss:
            if x=="[":
                c += 1
            elif x==']':
                c -= 1
                if c<0: return False
        return c == 0

    for bp in lst:
        idx = None
        for i, t in enumerate(cs):
            if compatible(bp,t):
                idx = i
                break
        if idx is None:
            idx = len(cs)
            cs.append(list("."*len(target)))

        cs[idx][bp[0]]="["
        cs[idx][bp[1]]="]"

    return [ "".join(s) for s in cs ]

def print_param(args):
    to_avoid = ['nSolutions', 'seed', 'verbose', 'show_progress', 'target', 'varna']
    argsDist = vars(args)
    res = " | ".join(["{}: {}".format(k,v) for k,v in argsDist.items() if k not in to_avoid])
    LOG.debug(res)

def print_added_constraints(length, bps):
    c = ["."]*length
    for i,j in bps:
        c[i] = '['
        c[j] = ']'
    return ''.join(c)


def generate_incompatibility_constraints(target):
    """
    Generate incompatibility constraints to facilitate the design of the given target structure

    @param target structure as dot bracket string
    @return list of base pairs for incompatibility constraints
    """

    bps = ir.rna.parseRNAStructureBps(target)

    length = len(target)
    
    forbid = list()

    ## inner limits of helices
    forbid.extend(
        [ (i+1,j-1) for (i,j) in bps if i+1 < j-1-3 and (i+1,j-1) not in bps ]
    )

    ## outer limits of helices
    forbid.extend(
        [ (i-1,j+1) for (i,j) in bps if i>0 and j+1<length and (i-1,j+1) not in bps ]
    )

    forbid = list(set(forbid))

    return forbid



def print_dist(dist, f):
    for l in dist:
        print(' '.join(map(str,l)), file=f)

def count_dist_level(dist):
    res = {'0': 0, '1': 0, '2': 0, '3-5': 0, '6-10': 0, '11-20': 0, '21-': 0}
    for d in dist:
        if d == 0:
            res['0'] += 1
        elif d == 1:
            res['1'] += 1
        elif d == 2:
            res['2'] += 1
        elif d <= 5:
            res['3-5'] += 1
        elif d <= 10:
            res['6-10'] += 1
        elif d <= 20:
            res['11-20'] += 1
        else:
            res['21-'] += 1
    return res

def print_dist_summary(dist):
    return "\n  ".join(["#Sampled with distance at"] + ["{}: {}".format(k, v) for k, v in count_dist_level(dist).items()])


def print_solution(seq, v):
    with Path(args.output, "solutions.txt").open(mode='a') as f:
        # s = "\t".join([seq, "bp-dist={}".format(v), "{:.4f}".format(time()-START)])
        s = "\t".join([seq, "bp-dist={}".format(v)])
        print(s, file=f)
        LOG.info(s)

def main(args):
    target = args.target
    LOG.info(target)
    seed = args.seed
    if seed is None:
        seed = random.randint(0, 2**31)
    ir.seed(seed)
    LOG.info("seed: {}".format(seed))
    print_param(args)

    gcw = args.gcweight
    hvyw = args.heavyDinucweight

    ir.rna.set_bpenergy_table()
    ir.rna.set_stacking_energy_table()

    targetBps = ir.rna.parseRNAStructureBps(target)
    to_check = set(targetBps)
    alldistList = []

    top = 0

    try:
        disruptive = json.loads(args.dbps)
        disruptive = [(i, j) for [i, j] in disruptive]
    except:
        LOG.error("Incompatibility constraints must be given as list of base pairs [i,j].")
        sys.exit(-1)

    if not args.no_initial_dbps:
        generated_disruptive = generate_incompatibility_constraints(target)
        disruptive.extend(generated_disruptive)

    disruptive = list(set(disruptive))

    for (i,j) in disruptive:
        to_check.add((i,j))

    if disruptive != []:
        LOG.debug("Initially disruptive: {}".format(disruptive))
        LOG.debug(target)
        LOG.debug('\n'.join(conflict_strings(target, disruptive)))

    sampler = NegativeSampler(target, gcw, disruptive, hvyw=hvyw)
    sample_generator = sampler.samples()
    sol = []
    min_dist = 6
    successCounter = 0
    stop_update = False
    indRound = -1

    while True:
        indRound += 1
        LOG.debug("Round {}".format(indRound))
        if not stop_update:
            LOG.debug("Disruptive bps: {}".format(sampler.disruptiveBpList))
        fstats = ir.FeatureStatistics()

        sample_count = 0
        zero_dist_count = 0
        sampledSeq = []
        distList = []

        bpCount = BPCount(method=args.method)
        for ind, seq in enumerate(sample_generator):
            if args.show_progress:
                print("Round{}: Seq".format(indRound), ind, end="\r")
            sampledSeq.append(seq)
            if successCounter > 0:
                successCounter += 1
            ss = RNA.fold(seq)[0]
            feat_id, value = fstats.record(BpDistFeature(target), ss)
            distList.append(value)
            if value == 0:
                sol.append(seq)
                min_dist = 0
                zero_dist_count += 1
                successCounter = max(1, successCounter)
                if successCounter == 1:
                    LOG.info("First solution found at round {}".format(indRound))
                print_solution(seq, value)
                if len(sol) >= args.nSolutions:
                    LOG.info("Success. {} solutions found in {} sampled sequence after the first solution".format(len(sol), successCounter))

                    if (not stop_update) and args.draw_target:
                        draw_structure(target, sampler.disruptiveBpList, Path(args.output,'structure.png'), args.varna)
                    sys.exit(-1)

            elif value < min_dist:
                print_solution(seq, value)
                min_dist = value

            if value > top:
                top = value
            feat_id, value = fstats.record(EnergyFeature(target), seq)

            if zero_dist_count >= args.min_sol and not stop_update:
                stop_update = True
                LOG.info("Sampler updating stops at round {}".format(indRound))
                if args.draw_target:
                    draw_structure(target, sampler.disruptiveBpList, Path(args.output,'structure.png'), args.varna)

            # Stop adding DBPs if minimum solutions found
            if not stop_update:
                bpCount.update(seq, ss)

            sample_count += 1
            if sample_count >= args.nSamples:
                break

        # Write down sampled sequences and their distance to target
        if args.verbose:
            with Path(args.output, "distance.txt").open(mode='a') as f:
                print(' '.join(map(str, distList)), file=f)

            with Path(args.output, 'samples', 'round_{}'.format(indRound)).open(mode='w') as f:
                for w in sampledSeq:
                    print(w, file=f)

            draw_structure(target, sampler.disruptiveBpList, Path(args.output,'structure_round_{}.png'.format(indRound)), args.varna)

        alldistList.append(distList)
        LOG.debug("Summary of sampling:")
        LOG.debug(print_dist_summary(distList))
        LOG.debug(fstats.report())

        if stop_update:
            continue

        # Update sampler
        nBps = 0
        updated = False
#        if args.print_bpp:
#            f = open('bpp.txt', 'w')
#            for k,v in bpCount.counter.items():
#                print(k[0], k[1], v/args.nSamples, file=f)
#            f.close()
        for bp, v in bpCount.get_bp():
            if bp in to_check:
                continue
            LOG.debug("bp: {} value: {}".format(bp, v))
            lst = sampler.disruptiveBpList[:]
            lst.append(bp)

            new_sampler = NegativeSampler(target, gcw, lst, hvyw=hvyw)
            to_check.add(bp)
            if new_sampler.is_consistent():
                nBps += 1
                sampler = new_sampler
                sample_generator = new_sampler.samples()
                updated = True
                LOG.debug("Treewidth: {}".format(sampler.treewidth()))
                # Stop when treewidth too large
                if sampler.treewidth() > args.treewidth:
                    if args.draw_target:
                        draw_structure(target, sampler.disruptiveBpList, Path(args.output, 'structure.png'), args.varna)
                    if len(sol) > 0:
                        LOG.info("Success. {} solutions found in {} sampled sequence after the first solution\n Stop due to large Treewidth".format(len(sol), successCounter))
                    else:
                        LOG.info("Fail. Treewidth too large")
                    sys.exit(-1)
            else:
                LOG.debug("Reject {} due to inconsistency".format(bp))

            if nBps >= args.nBasepairs:
                break

        if updated:
            # Print target and incompatibility constraints
            LOG.debug(target)
            LOG.debug('\n'.join(conflict_strings(target, sampler.disruptiveBpList[-nBps:])))
            LOG.debug("{} new disruptive base pairs: {}".format(nBps, sampler.disruptiveBpList[-nBps:]))

        # Stop when ther's is no update available
        else:
            if args.draw_target:
                draw_structure(target, sampler.disruptiveBpList, Path(args.output, 'structure.png'), args.varna)
            if len(sol) > 0:
                LOG.info("Success. {} solutions found in {} sampled sequence after the first solution\n Stop due to large Treewidth".format(len(sol), successCounter))
            else:
                LOG.info("Fail. No more available update")
            sys.exit(-1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="Script to test idea for negative design"
    )

    parser.add_argument('-n', '--nSolutions', type=int, default=10,
                        help="Numer of design")
    parser.add_argument('-s', '--nSamples', type=int, default=200,
                        help="Number of samples at each round")
    parser.add_argument('-tw', '--treewidth', type=int, default=10,
                        help="Maximum tree width allowed")
#    parser.add_argument('-r', '--nRounds', type=int,
#                        default=10, help="Maximum number of rounds")
    parser.add_argument('-b', '--nBasepairs', type=int, default=3,
                        help="Number of base pairs to forbid at each round")
    parser.add_argument('--seed', type=int, default=None, help='Random generation seed')
    parser.add_argument('-m', '--method', choices=['bpp', 'count'], default='bpp', help='Rule for dbps selection')
    parser.add_argument('--turner1999', action='store_true', help='Enable Turner1999 energy model')
    parser.add_argument('--verbose', action='store_true', help='Verbose mode')
    parser.add_argument('--gcweight', type=float, default=0.2,
                        help="GC control for unpaired bases. Set 1 to disable")
    parser.add_argument('--heavyDinucweight', type=float, default=None,
                        help="GC dinucleotide control")
    parser.add_argument('--no_initial_dbps', action='store_true',
                        help='Disable initial disruptive base pairs generation')
    parser.add_argument('--dbps', type=str, default="[]",
                        help="Initial list of disruptive base pairs.")
    parser.add_argument('-o', '--output', type=str, default="./",
                        help="Output folder")
    parser.add_argument('--min_sol', type=int, default=1,
                        help="Minimum number solutions needed in one round to stop updating sampler")
#    parser.add_argument('--conflict_strings', action='store_true', help="Print conflict strings")
    parser.add_argument('--show_progress', action='store_true',help="Print progress counter")
    parser.add_argument('--draw_target', action='store_true', help='Draw target with dbps')
    parser.add_argument('--varna', default='VARNAv3-93.jar', help='VARNA location')
#    parser.add_argument('--print_bpp', action='store_true')
    parser.add_argument("target", metavar="Target",
                        help="Target secondary structure to design")

    args = parser.parse_args()

    if args.verbose:
        LOG.setLevel(logging.DEBUG)
    else:
        LOG.setLevel(logging.INFO)
    RNA.cvar.uniq_ML = 1
    if args.turner1999:
        p = Path(__file__).resolve().parent
        RNA.params_load(str(p.joinpath("rna_turner1999.par")))
    Path(args.output).mkdir(parents=True, exist_ok=True)
    c = Path(args.output, 'solutions.txt')
    if c.exists():
        c.unlink()
    if not Path(args.varna).exists():
        LOG.warn("Cannot find {}".format(args.varna))
    else:
        set_VARNA(args.varna)
    if args.verbose:
        Path(args.output, 'samples').mkdir(parents=True, exist_ok=True)
    main(args)
